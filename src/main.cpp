#include <Arduino.h>     // Arduino library
#include <ESP8266WiFi.h> // WiFi library

void setup() // Code in here only runs once.
{
    Serial.begin(9600);               // Set up a serial connection to the computer.
    Serial.print("Arduino started."); // Send a startup message!

    pinMode(D4, OUTPUT); // Set up pin D4 as an ouput.
}

void loop()
{
    int numNetworks = WiFi.scanNetworks(); // Scan for networks. This function returns the number of networks it found.
    // The info of each network can be found later using the WiFi object and using an index.
    // More documentation here: https://arduino-esp8266.readthedocs.io/en/latest/esp8266wifi/scan-class.html

    Serial.print("We found "); // Print the number of networks we found to the Serial console.
    Serial.print(numNetworks);
    Serial.println(" networks!");

    for (int i = 0; i < numNetworks; i++) // Loop through all the networks. Current network stored in i
    {
        String wifiName = WiFi.SSID(i);         // Get the name of network i
        if (wifiName.equals("WWU MTC Arduino")) // Check if network i is the network we want.
        {
            Serial.println("Found it!"); // Let the console know the good news!
            digitalWrite(D4, HIGH);      // LED on
            return;                      // Return exists the current function, so we stop our for loop and never hit the digitalWrite at the bottom.
        }
    }
    digitalWrite(D4, LOW); // LED off
}